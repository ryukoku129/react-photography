import React from "react";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import * as ROUTES from "./constants/routes";
import logo from "./logo.svg";
import "./App.css";
import ViewCard from "./pages/List";
import Header from "./components/Header";
import Login from "./pages/SignIn/Login";
import Home from "./pages/Home/Home";
import List from "./pages/List/List";
import Post from "./pages/Post/Post";
import Messagebox from "./pages/Addtofb/Messagebox";
import withFirebaseAuth from "react-with-firebase-auth";
import * as firebase from "firebase/app";
import "firebase/auth";
import firebaseConfig from "./components/firebase/firebaseConfig";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import Cardtemplate from "./pages/List";
const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAppAuth = firebaseApp.auth();
const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider()
};

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Router>
            <Header title="Snap" />
            <div>
              <Route exact path={ROUTES.LANDING} component={Cardtemplate} />
              <Route exact path={ROUTES.HOME} component={Home} />
              <Route exact path={ROUTES.SIGN_IN} component={Login} />
            </div>
          </Router>
        </header>

        {/* <List db={firebase} /> */}
        {/* <Messagebox db={firebase} />
        <ViewCard /> */}
      </div>
    );
  }
}
export default withFirebaseAuth({
  providers,
  firebaseAppAuth
})(App);
