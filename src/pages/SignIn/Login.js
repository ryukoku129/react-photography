import React, { Component } from "react";
import firebase from "firebase";
import "firebase/auth";
import "../../components/style/style.css";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import Post from "../Post/Post";
import MessageBox from "../Addtofb/Messagebox";
export default class Login extends Component {
  // The component's Local state.
  state = {
    isSignedIn: false // Local signed-in state.
  };
  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: "popup",
    // We will display Google , Facebook , Etc as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccess: () => false
    }
  };
  componentDidMount() {
    this.unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged(user => this.setState({ isSignedIn: !!user }));
  }

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }
  render() {
    if (!this.state.isSignedIn) {
      return (
        <div className="Topmargin">
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
        </div>
      );
    }
    return (
      <div className="container">
        <div className="columndisplay">
          <div className="user">
            <img
              id="photo"
              className="pic"
              src={firebase.auth().currentUser.photoURL}
            />
            <p>
              Welcome {firebase.auth().currentUser.displayName}! You are now
              signed-in!
            </p>
            <button
              className="fillcontainer"
              onClick={() => firebase.auth().signOut()}
            >
              Sign-out
            </button>
          </div>
          <div>
            <MessageBox db={firebase} />
          </div>
        </div>
      </div>
    );
  }
}
