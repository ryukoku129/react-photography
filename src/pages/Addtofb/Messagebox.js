import React, { Component } from "react";
import trim from "trim";
import firebase from "firebase";
import "firebase/auth";
class MessageBox extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onKeyup = this.onKeyup.bind(this);
    this.uploadable = true;
    this.state = {
      message: "",
      name: "",
      profileurl: ""
    };
  }

  onChange(e) {
    this.setState({
      message: e.target.value
    });
  }
  onKeyup(e) {
    if (firebase.auth().currentUser.uid) {
      if (this.uploadable && trim(e.target.value) !== "") {
        console.log(this.uploadable);

        e.preventDefault();
        let dbCon = this.props.db
          .database()
          .ref("Messages/Users/" + firebase.auth().currentUser.uid);
        dbCon.push({
          message: trim(e.target.value),
          name: firebase.auth().currentUser.displayName,
          profileurl: firebase.auth().currentUser.photoURL
        });
        this.setState({
          message: "",
          name: "",
          profileurl: ""
        });
      }
      // this.uploadable = false;
    }
  }

  render() {
    return (
      <div>
        <form>
          <textarea
            className="textarea"
            placeholder="Type a message"
            cols="100"
            onChange={this.onChange}
            onKeyUp={this.onKeyup}
            value={this.state.message}
          ></textarea>
        </form>
        <button>Submit</button>
      </div>
    );
  }
}

export default MessageBox;
