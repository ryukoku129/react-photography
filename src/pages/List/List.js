import React, { Component } from "react";
import Post from "../Post/Post";
import _ from "lodash";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import "../../App.css";
const useStyles = makeStyles(theme => ({
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  card: {
    maxWidth: 345
  }
}));
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: []
    };
    let app = this.props.db.database().ref("messages");
    app.on("value", snapshot => {
      this.getData(snapshot.val());
    });
  }
  getData(values) {
    let messagesVal = values;
    let messages = _(messagesVal)
      .keys()
      .map(messageKey => {
        let cloned = _.clone(messagesVal[messageKey]);
        cloned.key = messageKey;
        return cloned;
      })
      .value();
    this.setState({
      messages: messages
    });
  }
  render() {
    let messageNodes = this.state.messages.map(message => {
      return (
        <div className="card">
          <div className="card-content">
            <Post
              msgKey={message.key}
              message={message.message}
              db={this.props.db}
            />
            <h1>This is from function </h1>
            <Cardtemplate />
          </div>
        </div>
      );
    });
    return <div>{messageNodes}</div>;
  }
}

const Cardtemplate = () => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div className="App-header">
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className="avatar">
              R
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title=""
          subheader="September 14, 2016"
        />
        <CardMedia
          className="media"
          image="/Users/suprajakpetchdee/react-photography/public/logo512.png"
          title="Paella dish"
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            This impressive paella is a perfect party dish and a fun meal to
            cook together with your guests.Add 1 cup of frozen peas along with
            the mussels, if you like.{" "}
          </Typography>{" "}
        </CardContent>{" "}
        <CardActions disableSpacing>
          <IconButton aria-label="add to favorites">
            <FavoriteIcon />
          </IconButton>{" "}
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>{" "}
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>{" "}
        </CardActions>{" "}
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph> Method: </Typography>{" "}
            <Typography paragraph>
              Heat 1 / 2 cup of the broth in a pot until simmering, add saffron
              and set aside for 10 minutes.{" "}
            </Typography>{" "}
            <Typography paragraph> Text will import from database </Typography>{" "}
          </CardContent>{" "}
        </Collapse>{" "}
      </Card>{" "}
    </div>
  );
};

export default List;
