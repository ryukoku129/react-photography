import React, { Component } from 'react'
import { Route, Switch, Link, BrowserRouter as Router } from 'react-router-dom'

import Login from '../pages/SignIn/Login'
import App from '../App'
import * as ROUTES from '../constants/routes'

import './style/style.css'

import { fade, makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import InputBase from '@material-ui/core/InputBase'

import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import MoreIcon from '@material-ui/icons/MoreVert'


const routing = (
  <Router>
    <div>
      <Route path='/' component={App} />
      <Route path='/Login' component={Login} />
    </div>
  </Router>
)
const style = {
  background: '#000000',
  width: '100%'
}
const white = {
  color: 'ffffff'
}

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
    position: 'static'
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3)
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'relative',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#ffffff'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
}))

class Header extends Component {
  render () {
    const classes = useStyles
    const menuId = 'primary-search-account-menu'
    return (
      <div className='flexibleHeader'>
        <div className={classes.grow}>
          <AppBar style={style} position='fixed'>
            <Toolbar>
              <MenuIcon />
              <Link className='Add-left-for-header' to={ROUTES.LANDING}>
                SNAP
              </Link>
              <Link className='Add-left-for-header' to={ROUTES.LIST_PHOTO}>
                PHOTOGRAPHERS
              </Link>
              <Link className='Add-left-for-header' to={ROUTES.LIST_MODEL}>
                MODELS
              </Link>
              <div className="startfromright">
                <IconButton>
                  <SearchIcon style={white} />
                </IconButton>

                <InputBase
                  style={{ width: 200 }}
                  className='inputfiled'
                  placeholder='  Search…'
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                  inputProps={{ 'aria-label': 'search' }}
                />
                <IconButton
                  edge='end'
                  aria-label='account of current user'
                  aria-controls={menuId}
                  aria-haspopup='true'
                  color='inherit'
                  component={Link}
                  to={ROUTES.SIGN_IN}
                >
                  <AccountCircle />
                </IconButton>
              </div>
            </Toolbar>
          </AppBar>
        </div>
      </div>
    )
  }
}
export default Header
