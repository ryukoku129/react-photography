const prodConfig = {
  apiKey: 'AIzaSyDBRXInPbXZLBct3qy3MxIAIawyQVCFAXA',
  authDomain: 'photography-b7aa6.firebaseapp.com',
  databaseURL: 'https://photography-b7aa6.firebaseio.com',
  projectId: 'photography-b7aa6',
  storageBucket: 'photography-b7aa6.appspot.com',
  messagingSenderId: '271712882396',
  appId: '1:271712882396:web:e639307fefd173bd56ee19',
  measurementId: 'G-N35K8D9PS8'
}
const devConfig = {
  apiKey: 'AIzaSyDBRXInPbXZLBct3qy3MxIAIawyQVCFAXA',
  authDomain: 'photography-b7aa6.firebaseapp.com',
  databaseURL: 'https://photography-b7aa6.firebaseio.com',
  projectId: 'photography-b7aa6',
  storageBucket: 'photography-b7aa6.appspot.com',
  messagingSenderId: '271712882396',
  appId: '1:271712882396:web:e639307fefd173bd56ee19',
  measurementId: 'G-N35K8D9PS8'
}
const config = process.env.NODE_ENV === 'production' ? prodConfig : devConfig

export default config
